package shoppingcart;

/**
 *
 * @author mitpa
 */
public class Product 
{
    private String name;
    private double price;

    //arg constructor
    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    //getter 
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
    
    
    
}
