package shoppingcart;

/**
 *
 * @author mitpa
 */
public class PaymentServiceFactory {
   
    private static PaymentServiceFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private PaymentServiceFactory()
    {}
    
    public static PaymentServiceFactory getInstance()
    {
        if(factory==null)
            factory = new PaymentServiceFactory();
        return factory;
    }
   
    public PaymentService getPaymentService(PaymentServiceType type)
    {
        switch( type )
        {
            case DEBIT : return new DebitPaymentService(50);
            case CREDIT : return new CreditPaymentService(100);
        }
        
        return null;
    }
}


