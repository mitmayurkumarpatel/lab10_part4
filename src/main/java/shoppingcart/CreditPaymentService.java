/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shoppingcart;

/**
 *
 * @author mitpa
 */
public class CreditPaymentService extends PaymentService 
{

    public CreditPaymentService(double amount) {
        super(amount);
    }
    
  @Override
    public void processPayment(double amount) 
    {
        double output = amount;
        System.out.println("Processing credit payment of " +output);
        
    }
    
    
}
