package shoppingcart;

/**
 *
 * @author mitpa
 */
public abstract class PaymentService
{
    private  double amount;
   

    //constructor
    public PaymentService(double amount) {
        this.amount = amount;
    }

    //getter
    public double getAmount() {
        return amount;
    }
    
    public abstract void processPayment(double amount);
    
    
}
