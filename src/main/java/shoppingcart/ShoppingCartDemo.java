package shoppingcart;

/**
 *
 * @author mitpa
 */
public class ShoppingCartDemo 
{
    public static void main(String []args)
    {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentServiceFactory creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentServiceFactory debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        
        Cart cart = new Cart();
        cart.addProduct( new Product("shirt", 50));
        cart.addProduct( new Product("pants", 50));
        
        cart.setPaymentService( creditDervice );
        cart.payCart();
        
        cart.setPaymentService ( debitService );
        cart.payCart();
        
                
        
        DebitPaymentService d1 = new DebitPaymentService(50);
       d1.processPayment(50);
       
       CreditPaymentService c1 = new CreditPaymentService(100);
       c1.processPayment(100);
    }
    
}
