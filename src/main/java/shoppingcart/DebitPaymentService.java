package shoppingcart;

/**
 *
 * @author mitpa
 */
public class DebitPaymentService extends PaymentService
{

    public DebitPaymentService(double amount) {
        super(amount);
    }

    
    @Override
    public void processPayment(double amount) 
    {
        double output = amount;
        System.out.println("Processing credit payment of " +output);
        
    }
    
    
}
